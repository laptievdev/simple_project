

class TodoItemModel {
  final String title;
  final DateTime creationDate;

  TodoItemModel({this.title, this.creationDate});
}