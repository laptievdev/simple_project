import 'package:bloc_app/models/todo_item_model.dart';
import 'package:flutter/material.dart';

class AddTodoPage extends StatelessWidget {

  TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add a todo'),
      ),
      body: Builder(
        builder: (context) => SafeArea(
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  TextField(
                    controller: controller,
                    decoration: InputDecoration(
                      hintText: 'Add a todo'
                    ),
                  ),
                  SizedBox(height: 16,),
                  RaisedButton(
                    child: Text('Done'),
                    color: Colors.blue,
                    onPressed: () => _onReadyPressed(context),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  _onReadyPressed(BuildContext context) {
    if(controller.text.isEmpty) {
      Scaffold.of(context).showSnackBar(
        SnackBar(
          content: Text('Field must not be empty!'),
        )
      );
      return;
    }
    Navigator.pop(context, TodoItemModel(
      title: controller.text,
      creationDate: DateTime.now()
      )
    );
  }

}
