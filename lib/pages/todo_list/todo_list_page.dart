import 'package:bloc_app/models/todo_item_model.dart';
import 'package:bloc_app/pages/add_todo/add_todo_page.dart';
import 'package:flutter/material.dart';

class TodoListPage extends StatefulWidget {
  @override
  _TodoListPageState createState() => _TodoListPageState();
}

class _TodoListPageState extends State<TodoListPage> {

  List<TodoItemModel> _todos = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Todos'),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () => _onAddTodoPressed(),
      ),
      body: Builder(
        builder: (context) => SafeArea(
          child: ListView.builder(
            itemCount: _todos.length,
            itemBuilder: (context, index) => _buildTodoListTile(_todos[index])
          ),
        ),
      ),
    );
  }

  _buildTodoListTile(TodoItemModel model) => Card(
    child: ListTile(
      title: Text(model.title),
      subtitle: Text('Created at: ${model.creationDate.toIso8601String()}'),
    ),
  );

  _onAddTodoPressed() async {
    final todo = await Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => AddTodoPage()
      )
    );
    if (todo != null) {
      setState(() {
        _todos.add(todo);
      });
    }
  }

}
